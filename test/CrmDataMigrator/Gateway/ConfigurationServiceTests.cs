using System;
using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Gateway.Tests
{
    public class ConfigurationServiceTests
    {
        private readonly IConfiguration configuration;

        public ConfigurationServiceTests()
        {
            configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
        }

        [Fact]
        public async Task ConfigurePipedriveSmokeTest()
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            using var channel = GrpcChannel.ForAddress(GetGatewayAddress());
            var client =  new Configuration.ConfigurationClient(channel);
            var reply = await client.ConfigureAsync(new ConfigurationRequest
            {
                CrmService = ConfigurationRequest.Types.CrmService.Pipedrive,
                Pipedrive = GetPipedriveConfiguration()
            });
        }

        [Fact]
        public async Task ConfigureHubspotSmokeTest()
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            using var channel = GrpcChannel.ForAddress("http://localhost:50051");
            var client =  new Configuration.ConfigurationClient(channel);
            var reply = await client.ConfigureAsync(new ConfigurationRequest
            {
                CrmService = ConfigurationRequest.Types.CrmService.Hubspot,
                Hubspot = GetHubspotConfiguration()
            });
        }

        private PipedriveConfiguration GetPipedriveConfiguration()
        {
            return configuration.Get<PipedriveConfiguration>();
        }

        private HubspotConfiguration GetHubspotConfiguration()
        {
            return configuration.Get<HubspotConfiguration>();
        }

        private string GetGatewayAddress()
        {
            return configuration["address"];
        }
    }
}
