using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Tests
{
    public class DataConversionServiceTests
    {
        private readonly IConfiguration configuration;

        public DataConversionServiceTests()
        {
            configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
        }

        [Fact]
        public async Task PipedriveToHubspotSmokeTest()
        {
            var service = CreateService();
            var conversion = new Conversion
            {
                Input = new ConfigurationRequest
                {
                    CrmService = CrmService.Pipedrive,
                    Pipedrive = GetPipedriveConfiguration()
                },
                Output = new ConfigurationRequest
                {
                    CrmService = CrmService.Hubspot,
                    Hubspot = GetHubspotConfiguration()
                }
            };

            var conversionResult = await service.ConvertAsync(conversion);

            conversionResult.IsSuccessful.Should().BeTrue();
        }
        
        [Fact]
        public async Task HubspotToPipedriveSmokeTest()
        {
            var service = CreateService();
            var conversion = new Conversion
            {
                Input = new ConfigurationRequest
                {
                    CrmService = CrmService.Hubspot,
                    Hubspot = GetHubspotConfiguration()
                },
                Output = new ConfigurationRequest
                {
                    CrmService = CrmService.Pipedrive,
                    Pipedrive = GetPipedriveConfiguration()
                }
            };

            var conversionResult = await service.ConvertAsync(conversion);

            conversionResult.IsSuccessful.Should().BeTrue();
        }

        private HubspotConfiguration GetHubspotConfiguration()
        {
            return configuration.GetSection("hubspot").Get<HubspotConfiguration>();
        }

        private PipedriveConfiguration GetPipedriveConfiguration()
        {
            return configuration.GetSection("pipedrive").Get<PipedriveConfiguration>();
        }

        private static IDataConversionService CreateService()
        {
            var services = new ServiceCollection();
            services.AddDataConversionModule();
            var serviceProvider = services.BuildServiceProvider();
            var serviceScope = serviceProvider.CreateScope();
            return serviceScope.ServiceProvider.GetRequiredService<IDataConversionService>();
        }
    }
}
