using CrmDataMigrator.CrmModule.AccountVerification;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class CrmModuleServiceCollectionExtensions
    {
        public static IServiceCollection AddCrmModule(this IServiceCollection services)
        {
            services.AddHttp<ICrmAccountVerifier, PipedriveAccountVerifier>();
            services.AddHttp<ICrmAccountVerifier, HubspotAccountVerifier>();
            services.AddSingleton<IAccountVerifier, AccountVerifier>();
            return services;
        }
    }
}