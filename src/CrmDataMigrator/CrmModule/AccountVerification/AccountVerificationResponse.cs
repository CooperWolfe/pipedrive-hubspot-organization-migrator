namespace CrmDataMigrator.CrmModule.AccountVerification
{
    public class AccountVerificationResponse
    {
        private AccountVerificationResponse()
        {
        }

        public bool IsSuccessful { get; set; }
        public VerificationFailure? FailureReason { get; set; }

        public static AccountVerificationResponse Success()
        {
            return new AccountVerificationResponse
            {
                IsSuccessful = true
            };
        }

        public static AccountVerificationResponse Failure(VerificationFailure failureReason)
        {
            return new AccountVerificationResponse
            {
                IsSuccessful = false,
                FailureReason = failureReason
            };
        }

        public enum VerificationFailure
        {
            CouldNotContactService,
            Unauthorized,
            Unknown
        }
    }
}