﻿using System.Threading.Tasks;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.CrmModule.AccountVerification
{
    public interface IAccountVerifier
    {
        /// <summary>
        /// Verifies that the account exists
        /// </summary>
        /// <param name="configurationRequest">A pre-validated configuration object for the account</param>
        /// <returns>Whether the account truly exists</returns>
        Task<AccountVerificationResponse> VerifyAccountAsync(ConfigurationRequest configurationRequest);
    }
}
