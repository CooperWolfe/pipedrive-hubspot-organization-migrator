using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.CrmModule.AccountVerification
{
    internal class HubspotAccountVerifier : ICrmAccountVerifier
    {
        private readonly HttpClient httpClient;

        public HubspotAccountVerifier(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public CrmService CrmService => CrmService.Hubspot;

        public async Task<AccountVerificationResponse> VerifyAsync(ConfigurationRequest configuration)
        {
            var hubspotConfiguration = configuration.Hubspot;

            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await httpClient.GetAsync($"http://api.hubapi.com/crm/v3/owners?hapikey={hubspotConfiguration.ApiToken}");
            }
            catch (HttpRequestException)
            {
                return AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.CouldNotContactService);
            }

            switch (httpResponse.StatusCode)
            {
                case HttpStatusCode.OK:
                    return AccountVerificationResponse.Success();
                case HttpStatusCode.Unauthorized:
                    return AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.Unauthorized);
                default:
                    return AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.Unknown);
            }
        }
    }
}