using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.CrmModule.AccountVerification
{
    internal class AccountVerifier : IAccountVerifier
    {
        private readonly IDictionary<CrmService, ICrmAccountVerifier> verificationStrategies;

        public AccountVerifier(IEnumerable<ICrmAccountVerifier> verificationStrategies)
        {
            this.verificationStrategies = verificationStrategies.ToDictionary(verifier => verifier.CrmService);
        }

        public Task<AccountVerificationResponse> VerifyAccountAsync(ConfigurationRequest configurationRequest)
        {
            return verificationStrategies[configurationRequest.CrmService].VerifyAsync(configurationRequest);
        }
    }
}