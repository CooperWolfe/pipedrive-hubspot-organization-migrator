using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.CrmModule.AccountVerification
{
    internal class PipedriveAccountVerifier : ICrmAccountVerifier
    {
        private readonly HttpClient httpClient;

        public PipedriveAccountVerifier(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public CrmService CrmService => CrmService.Pipedrive;

        public async Task<AccountVerificationResponse> VerifyAsync(ConfigurationRequest configuration)
        {
            var pipedriveConfiguration = configuration.Pipedrive;

            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = await httpClient.GetAsync($"http://{pipedriveConfiguration.CompanyDomain}.pipedrive.com/api/v1/userSettings:(success)?api_token={pipedriveConfiguration.ApiToken}");
            }
            catch (HttpRequestException)
            {
                return AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.CouldNotContactService);
            }

            switch (httpResponse.StatusCode)
            {
                case HttpStatusCode.OK:
                    break;
                case HttpStatusCode.Unauthorized:
                    return AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.Unauthorized);
                default:
                    return AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.Unknown);
            }

            var jsonResponse = await httpResponse.Content.ReadAsStringAsync();
            var pipelineResponse = JsonSerializer.Deserialize<PipelineResponse>(jsonResponse);

            return pipelineResponse.Success
                ? AccountVerificationResponse.Success()
                : AccountVerificationResponse.Failure(AccountVerificationResponse.VerificationFailure.Unknown);
        }

        private class PipelineResponse
        {
            [JsonPropertyName("success")]
            public bool Success { get; set; }
        }
    }
}