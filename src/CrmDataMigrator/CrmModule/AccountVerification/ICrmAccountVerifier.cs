using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.CrmModule.AccountVerification
{
    internal interface ICrmAccountVerifier
    {
        CrmService CrmService { get; }
        Task<AccountVerificationResponse> VerifyAsync(ConfigurationRequest configuration);
    }
}