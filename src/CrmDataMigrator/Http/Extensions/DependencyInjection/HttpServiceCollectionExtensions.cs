﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Extensions.Http;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class HttpServiceCollectionExtensions
    {
        public static IServiceCollection AddHttp<TClient, TImplementation>(this IServiceCollection services)
            where TClient : class
            where TImplementation : class, TClient
        {
            services.AddHttpClient<TClient, TImplementation>().AddPolicyHandler(GetDefaultPolicy());
            return services;
        }

        private static IAsyncPolicy<HttpResponseMessage> GetDefaultPolicy()
        {
            IAsyncPolicy<HttpResponseMessage> retryPolicy = GetRetryPolicy(5, TimeSpan.FromSeconds(1));
            IAsyncPolicy<HttpResponseMessage> circuitBreakerPolicy = GetCircuitBreakerPolicy(5, TimeSpan.FromMinutes(1));

            return Policy.WrapAsync(retryPolicy, circuitBreakerPolicy);
        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy(int retryCount, TimeSpan medianFirstRetryDelay)
        {
            IEnumerable<TimeSpan> jitterrer = Backoff.DecorrelatedJitterBackoffV2(medianFirstRetryDelay, retryCount: retryCount);
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(jitterrer);
        }

        private static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy(int handledEventsAllowedBeforeBreaking, TimeSpan circuitBreakerDuration)
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(handledEventsAllowedBeforeBreaking: handledEventsAllowedBeforeBreaking, durationOfBreak: circuitBreakerDuration);
        }
    }
}
