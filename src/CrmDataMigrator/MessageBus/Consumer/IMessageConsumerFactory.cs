namespace CrmDataMigrator.MessageBus.Consumer
{
    public interface IMessageConsumerFactory
    {
        IMessageConsumer Create(string topic);
    }
}