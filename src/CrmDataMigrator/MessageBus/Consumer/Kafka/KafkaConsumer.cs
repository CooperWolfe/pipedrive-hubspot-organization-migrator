using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using CrmDataMigrator.MessageBus.Consumer.Exceptions;

namespace CrmDataMigrator.MessageBus.Consumer.Kafka
{
    internal class KafkaConsumer : IMessageConsumer
    {
        private readonly string topic;
        private readonly IConsumer<Ignore, string> consumer;

        public KafkaConsumer(string topic, IConsumer<Ignore, string> consumer)
        {
            this.topic = topic;
            this.consumer = consumer;
            consumer.Subscribe(topic);
        }

        public async Task CommitMessageAsync(CancellationToken stoppingToken)
        {
            await Task.Yield(); // Kafka happens to implement this synchronously
            consumer.Commit();
        }

        public async Task<IConsumptionResult> ConsumeAsync(CancellationToken stoppingToken)
        {
            await Task.Yield(); // Kafka happens to implement this synchronously
            try
            {
                var result = consumer.Consume(stoppingToken);
                return new KafkaConsumptionResult(result);
            }
            catch (ConsumeException exception)
            {
                throw new ConsumptionFailedException(exception);
            }
        }

        public async ValueTask DisposeAsync()
        {
            await Task.Yield(); // Kafka happens to implement this synchronously
            consumer.Dispose();
        }
    }
}