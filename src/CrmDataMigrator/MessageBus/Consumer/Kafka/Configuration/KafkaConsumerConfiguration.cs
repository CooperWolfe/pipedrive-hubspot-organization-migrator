using Confluent.Kafka;

namespace CrmDataMigrator.MessageBus.Consumer.Kafka.Configuration
{
    internal class KafkaConsumerConfiguration
    {
        public string GroupId { get; set; }
        public string BootstrapServers { get; set; }

        public ConsumerConfig ToConsumerConfig()
        {
            return new ConsumerConfig
            {
                EnableAutoCommit = false,
                GroupId = GroupId,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                AllowAutoCreateTopics = true,
                BootstrapServers = BootstrapServers
            };
        }
    }
}