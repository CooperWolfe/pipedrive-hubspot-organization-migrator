using System.Text.Json;
using Confluent.Kafka;
using CrmDataMigrator.MessageBus.Consumer.Exceptions;

namespace CrmDataMigrator.MessageBus.Consumer.Kafka
{
    internal class KafkaConsumptionResult : IConsumptionResult
    {
        private readonly string message;

        public KafkaConsumptionResult(ConsumeResult<Ignore, string> result)
        {
            IsSuccessful = true;
            message = result.Message.Value;
        }

        public bool IsSuccessful { get; set; }

        public ConsumptionFailedException Exception { get; set; }

        public TMessage GetMessage<TMessage>()
        {
            return JsonSerializer.Deserialize<TMessage>(message);
        }
    }
}