using Confluent.Kafka;
using CrmDataMigrator.MessageBus.Consumer.Kafka.Configuration;
using Microsoft.Extensions.Options;

namespace CrmDataMigrator.MessageBus.Consumer.Kafka
{
    internal class KafkaConsumerFactory : IMessageConsumerFactory
    {
        private readonly ConsumerConfig configuration;

        public KafkaConsumerFactory(IOptions<KafkaConsumerConfiguration> options)
        {
            configuration = options.Value.ToConsumerConfig();
        }

        public IMessageConsumer Create(string topic)
        {
            var consumer = new ConsumerBuilder<Ignore, string>(configuration).Build();
            return new KafkaConsumer(topic, consumer);
        }
    }
}