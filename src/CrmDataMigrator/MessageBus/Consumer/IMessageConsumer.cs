using System;
using System.Threading;
using System.Threading.Tasks;

namespace CrmDataMigrator.MessageBus.Consumer
{
    public interface IMessageConsumer : IAsyncDisposable
    {
        Task<IConsumptionResult> ConsumeAsync(CancellationToken stoppingToken);
        Task CommitMessageAsync(CancellationToken stoppingToken);
    }
}