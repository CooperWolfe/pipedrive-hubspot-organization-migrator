using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CrmDataMigrator.MessageBus.Consumer
{
    public abstract class MessageBusConsumerBackgroundService<TMessage> : BackgroundService
    {
        private readonly string topic;
        private readonly ILogger<MessageBusConsumerBackgroundService<TMessage>> logger;
        private readonly IMessageConsumerFactory consumerFactory;
        
        private IMessageConsumer consumer;

        protected MessageBusConsumerBackgroundService(
            string topic,
            ILogger<MessageBusConsumerBackgroundService<TMessage>> logger,
            IMessageConsumerFactory consumerFactory)
        {
            this.topic = topic;
            this.logger = logger;
            this.consumerFactory = consumerFactory;
            this.consumer = consumerFactory.Create(topic);
        }

        protected abstract Task<bool> OnException(Exception exception);
        protected abstract Task<bool> OnConsume(TMessage message, CancellationToken stoppingToken);

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Yield();

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var consumeResult = await consumer.ConsumeAsync(stoppingToken);
                    if (!consumeResult.IsSuccessful)
                    {
                        throw consumeResult.Exception;
                    }

                    var message = consumeResult.GetMessage<TMessage>();
                    if (await OnConsume(message, stoppingToken))
                    {
                        await consumer.CommitMessageAsync(stoppingToken);
                    }
                }
                catch (Exception exception)
                {
                    logger.LogError(exception, "An error occurred while trying to consume message");
                    if (await OnException(exception))
                    {
                        await consumer.CommitMessageAsync(stoppingToken);
                    }

                    await consumer.DisposeAsync();
                    consumer = consumerFactory.Create(topic);
                }
            }

            await consumer.DisposeAsync();
        }
    }
}