using System;

namespace CrmDataMigrator.MessageBus.Consumer.Exceptions
{
    public class ConsumptionFailedException : Exception
    {
        public ConsumptionFailedException(Exception innerException) : base(innerException.Message)
        {
        }
    }
}