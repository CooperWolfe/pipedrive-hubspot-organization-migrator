using System;
using CrmDataMigrator.MessageBus.Consumer.Exceptions;

namespace CrmDataMigrator.MessageBus.Consumer
{
    public interface IConsumptionResult
    {
        bool IsSuccessful { get; }
        ConsumptionFailedException Exception { get; }
        TMessage GetMessage<TMessage>();
    }
}