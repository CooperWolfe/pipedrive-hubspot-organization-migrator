using CrmDataMigrator.MessageBus.Consumer;
using CrmDataMigrator.MessageBus.Consumer.Kafka;
using CrmDataMigrator.MessageBus.Consumer.Kafka.Configuration;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MessageBusServiceCollectionExtensions
    {
        public static IServiceCollection AddMessageBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<KafkaConsumerConfiguration>().Bind(configuration.GetSection("KAFKA"));
            services.AddSingleton<IMessageConsumerFactory, KafkaConsumerFactory>();
            return services;
        }
    }
}