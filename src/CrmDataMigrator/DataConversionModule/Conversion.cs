﻿using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule
{
    public class Conversion
    {
        public ConfigurationRequest Input { get; set; }
        public ConfigurationRequest Output { get; set; }
    }
}
