using System.Collections.Generic;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Ingress.Hubspot
{
    internal class HubspotIngressService : CrmIngressService
    {
        public HubspotIngressService(IEnumerable<IHubspotQuery> queries) : base(queries)
        {
        }

        public override CrmService CrmService => CrmService.Hubspot;
    }
}