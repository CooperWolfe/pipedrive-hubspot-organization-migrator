using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Hubspot;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Ingress.Hubspot
{
    internal class CompaniesQuery : IHubspotQuery
    {
        private readonly HttpClient httpClient;

        public CompaniesQuery(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Hubspot;

            var httpRequest = new HttpRequestMessage(
                HttpMethod.Get,
                $"https://api.hubapi.com/crm/v3/objects/companies" +
                    $"?hapikey={configuration.ApiToken}");
            var httpResponse = await httpClient.SendAsync(httpRequest);

            await using var stream = await httpResponse.Content.ReadAsStreamAsync();
            var companiesResponse = await JsonSerializer.DeserializeAsync<ReadCompanies>(stream);
            var companies = companiesResponse.Results;
            var organizations = companies.Select(company => company.ToProprietaryOrganization());

            data.Organizations = organizations.ToArray();
        }
    }
}