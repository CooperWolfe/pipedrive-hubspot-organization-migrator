using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Hubspot;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Ingress.Hubspot
{
    internal class CompanyAssociationsQuery : IHubspotQuery
    {
        private readonly HttpClient httpClient;

        public CompanyAssociationsQuery(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Hubspot;

            var getRelationships = data.Organizations.Select(async organization =>
            {
                var httpRequest = new HttpRequestMessage(
                    HttpMethod.Get,
                    $"https://api.hubapi.com/crm/v3/objects/companies/{organization.IngressId}/associations/company" +
                        $"?hapikey={configuration.ApiToken}");
                var httpResponse = await httpClient.SendAsync(httpRequest);

                await using var stream = await httpResponse.Content.ReadAsStreamAsync();
                var companyAssociationsResponse = await JsonSerializer.DeserializeAsync<ReadCompanyAssociations>(stream);
                return companyAssociationsResponse.Results
                    .Select(association => association.ToProprietaryOrganizationRelationship(organization.IngressId));
            });

            IEnumerable<IEnumerable<OrganizationRelationship>> organizationRelationships;
            try
            {
                organizationRelationships = await Task.WhenAll(getRelationships);
            }
            catch (Exception ex)
            {
                IEnumerable<Exception> exceptions = getRelationships
                    .Where(task => task.IsFaulted)
                    .Select(task => task.Exception);
                if (!exceptions.Any())
                {
                    exceptions = new[] { ex };
                }
                throw new IngressException(exceptions);
            }

            data.OrganizationRelationships = organizationRelationships
                .SelectMany(setOfRelationships => setOfRelationships)
                .Distinct()
                .ToArray();
        }
    }
}