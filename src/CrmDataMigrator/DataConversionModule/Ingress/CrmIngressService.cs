using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Ingress
{
    internal abstract class CrmIngressService : ICrmIngressService
    {
        private readonly IEnumerable<ICrmQuery> queries;

        protected CrmIngressService(IEnumerable<ICrmQuery> queries)
        {
            this.queries = queries;
        }

        public abstract CrmService CrmService { get; }

        public async Task<IngressResponse> ImportAsync(ConfigurationRequest configuration)
        {
            var crmData = new CrmData();

            foreach (var query in queries)
            {
                try
                {
                    await query.ExecuteAsync(configuration, crmData);
                }
                catch (IngressException ex)
                {
                    return IngressResponse.Fail(ex);
                }
                catch (Exception ex)
                {
                    var ingressException = new IngressException(ex);
                    return IngressResponse.Fail(ingressException);
                }
            }

            return IngressResponse.Success(crmData);
        }
    }
}