using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Ingress
{
    internal class IngressService : IIngressService
    {
        private readonly IDictionary<CrmService, ICrmIngressService> ingressServices;

        public IngressService(IEnumerable<ICrmIngressService> ingressServices)
        {
            this.ingressServices = ingressServices.ToDictionary(svc => svc.CrmService);
        }

        public async Task<IngressResponse> ImportAsync(ConfigurationRequest configuration)
        {
            if (ingressServices.TryGetValue(configuration.CrmService, out ICrmIngressService ingressService))
            {
                return await ingressService.ImportAsync(configuration);
            }
            else
            {
                return IngressResponse.Fail(new IngressException("CRM service ingress unsupported"));
            }
        }
    }
}