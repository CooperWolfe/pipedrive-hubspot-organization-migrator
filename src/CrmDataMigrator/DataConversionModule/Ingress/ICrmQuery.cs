using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Ingress
{
    internal delegate void PopulateCrmData(CrmData data);

    internal interface ICrmQuery
    {
        Task ExecuteAsync(ConfigurationRequest configuration, CrmData data);
    }
}