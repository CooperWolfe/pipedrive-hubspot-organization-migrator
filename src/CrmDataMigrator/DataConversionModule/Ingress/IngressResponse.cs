using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;

namespace CrmDataMigrator.DataConversionModule.Ingress
{
    internal class IngressResponse
    {
        private IngressResponse()
        {
        }

        public CrmData Data { get; set; }
        public IngressException Exception { get; set; }
        public bool IsSuccessful { get; set; }

        public static IngressResponse Success(CrmData crmData)
        {
            return new IngressResponse
            {
                IsSuccessful = true,
                Data = crmData
            };
        }

        internal static IngressResponse Fail(IngressException exception)
        {
            return new IngressResponse
            {
                IsSuccessful = false,
                Exception = exception
            };
        }
    }
}