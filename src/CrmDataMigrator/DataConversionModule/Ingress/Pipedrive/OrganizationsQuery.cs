using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Pipedrive;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Ingress.Pipedrive
{
    internal class OrganizationsQuery : IPipedriveQuery
    {
        private readonly HttpClient httpClient;

        public OrganizationsQuery(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Pipedrive;

            var httpRequest = new HttpRequestMessage(
                HttpMethod.Get,
                $"https://{configuration.CompanyDomain}.pipedrive.com/v1/organizations?api_token={configuration.ApiToken}");
            var httpResponse = await httpClient.SendAsync(httpRequest);

            await using var stream = await httpResponse.Content.ReadAsStreamAsync();
            var organizationsResponse = await JsonSerializer.DeserializeAsync<ReadOrganizations>(stream);
            var organizations = organizationsResponse.Data;
            var proprietaryOrganizations = organizations
                .Select(organizations => organizations.ToProprietaryOrganization())
                .ToArray();

            data.Organizations = proprietaryOrganizations;
        }
    }
}