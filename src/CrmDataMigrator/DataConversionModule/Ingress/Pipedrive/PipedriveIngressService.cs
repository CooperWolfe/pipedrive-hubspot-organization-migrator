using System.Collections.Generic;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Ingress.Pipedrive
{
    internal class PipedriveIngressService : CrmIngressService
    {
        public PipedriveIngressService(IEnumerable<IPipedriveQuery> queries) : base(queries) {}
        public override CrmService CrmService => CrmService.Pipedrive;
    }
}