using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Pipedrive;
using CrmDataMigrator.Grpc;
using ProprietaryOrganizationRelationship = CrmDataMigrator.DataConversionModule.Model.OrganizationRelationship;

namespace CrmDataMigrator.DataConversionModule.Ingress.Pipedrive
{
    internal class OrganizationRelationshipsQuery : IPipedriveQuery
    {
        private readonly HttpClient httpClient;

        public OrganizationRelationshipsQuery(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        
        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Pipedrive;

            var getOrganizations = data.Organizations.Select(async organization =>
            {
                var httpRequest = new HttpRequestMessage(
                    HttpMethod.Get,
                    $"https://{configuration.CompanyDomain}.pipedrive.com/v1/organizationRelationships" +
                        $"?api_token={configuration.ApiToken}" +
                        $"&org_id={organization.IngressId}");
                var httpResponse = await httpClient.SendAsync(httpRequest);

                await using var stream = await httpResponse.Content.ReadAsStreamAsync();
                var organizationsResponse = await JsonSerializer.DeserializeAsync<ReadOrganizationRelationships>(stream);
                var proprietaryOrganizationRelationships = organizationsResponse.Data?
                    .Select(or => or.ToProprietaryOrganizationRelationship())
                    ?? Enumerable.Empty<ProprietaryOrganizationRelationship>();
                return proprietaryOrganizationRelationships;
            });

            IEnumerable<IEnumerable<ProprietaryOrganizationRelationship>> organizationRelationships;
            try
            {
                organizationRelationships = await Task.WhenAll(getOrganizations);
            }
            catch (Exception ex)
            {
                IEnumerable<Exception> exceptions = getOrganizations
                    .Where(task => task.IsFaulted)
                    .Select(task => task.Exception);
                if (!exceptions.Any())
                {
                    exceptions = new[] { ex };
                }
                throw new IngressException(exceptions);
            }

            data.OrganizationRelationships = organizationRelationships
                .SelectMany(setOfRelationships => setOfRelationships)
                .Distinct()
                .ToArray();
        }
    }
}