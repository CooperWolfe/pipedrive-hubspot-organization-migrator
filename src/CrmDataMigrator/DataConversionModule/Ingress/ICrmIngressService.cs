using System.Threading.Tasks;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Ingress
{
    interface ICrmIngressService
    {
        CrmService CrmService { get; }
        Task<IngressResponse> ImportAsync(ConfigurationRequest configuration);
    }
}