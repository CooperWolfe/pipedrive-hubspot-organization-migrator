using System.Threading.Tasks;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Ingress
{
    internal interface IIngressService
    {
        Task<IngressResponse> ImportAsync(ConfigurationRequest configuration);
    }
}