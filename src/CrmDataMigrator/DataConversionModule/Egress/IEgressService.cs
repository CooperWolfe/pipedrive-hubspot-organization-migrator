using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress
{
    internal interface IEgressService
    {
        Task<EgressResponse> ExportAsync(ConfigurationRequest configuration, CrmData data);
    }
}