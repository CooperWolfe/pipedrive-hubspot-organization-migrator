using CrmDataMigrator.DataConversionModule.Exceptions;

namespace CrmDataMigrator.DataConversionModule.Egress
{
    class EgressResponse
    {
        private EgressResponse()
        {
        }

        public EgressException Exception { get; set; }
        public bool IsSuccessful { get; set; }

        public static EgressResponse Success()
        {
            return new EgressResponse
            {
                IsSuccessful = true
            };
        }

        public static EgressResponse Fail(EgressException exception)
        {
            return new EgressResponse
            {
                IsSuccessful = false,
                Exception = exception
            };
        }
    }
}