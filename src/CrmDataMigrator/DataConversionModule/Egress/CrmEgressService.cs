using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Egress
{
    internal abstract class CrmEgressService : ICrmEgressService
    {
        private readonly IEnumerable<ICrmCommand> commands;

        protected CrmEgressService(IEnumerable<ICrmCommand> commands)
        {
            this.commands = commands;
        }

        public abstract CrmService CrmService { get; }

        public async Task<EgressResponse> ExportAsync(ConfigurationRequest configuration, CrmData data)
        {
            foreach (var command in commands)
            {
                try
                {
                    await command.ExecuteAsync(configuration, data);
                }
                catch (EgressException ex)
                {
                    return EgressResponse.Fail(ex);
                }
                catch (Exception ex)
                {
                    var egressException = new EgressException(ex);
                }
            }

            return EgressResponse.Success();
        }
    }
}