using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Egress
{
    interface ICrmEgressService
    {
        CrmService CrmService { get; }
        Task<EgressResponse> ExportAsync(ConfigurationRequest configuration, CrmData data);
    }
}