using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Egress
{
    internal class EgressService : IEgressService
    {
        private readonly IDictionary<CrmService, ICrmEgressService> egressServices;

        public EgressService(IEnumerable<ICrmEgressService> egressServices)
        {
            this.egressServices = egressServices.ToDictionary(svc => svc.CrmService);
        }

        public async Task<EgressResponse> ExportAsync(ConfigurationRequest configuration, CrmData data)
        {
            if (egressServices.TryGetValue(configuration.CrmService, out ICrmEgressService egressService))
            {
                return await egressService.ExportAsync(configuration, data);
            }
            else
            {
                return EgressResponse.Fail(new EgressException("CRM service egress unsupported"));
            }
        }
    }
}