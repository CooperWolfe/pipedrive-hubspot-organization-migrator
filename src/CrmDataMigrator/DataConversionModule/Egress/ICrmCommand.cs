using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress
{
    internal interface ICrmCommand
    {
        Task ExecuteAsync(ConfigurationRequest configuration, CrmData data);
    }
}