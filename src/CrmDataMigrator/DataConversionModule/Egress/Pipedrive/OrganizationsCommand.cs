using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Pipedrive;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress.Pipedrive
{
    internal class OrganizationsCommand : IPipedriveCommand
    {
        private readonly HttpClient httpClient;

        public OrganizationsCommand(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Pipedrive;
            var createOrganizations = data.Organizations.Select(async organization =>
            {
                var createOrganization = CreateOrganizationRequest.FromProprietaryOrganization(organization);

                var httpRequest = new HttpRequestMessage(
                    HttpMethod.Post,
                    $"https://{configuration.CompanyDomain}.pipedrive.com/v1/organizations?api_token={configuration.ApiToken}");
                var jsonBody = JsonSerializer.Serialize(createOrganization);
                httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                var httpResponse = await httpClient.SendAsync(httpRequest);
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception($"Create organization unsuccessful ({organization.IngressId}): {httpResponse.StatusCode}");
                }
                await using var responseContent = await httpResponse.Content.ReadAsStreamAsync();
                var createOrganizationResponse = await JsonSerializer.DeserializeAsync<CreateOrganizationResponse>(responseContent);
                
                organization.EgressId = createOrganizationResponse.Data.Id.ToString();
                var fromRelationships = data.OrganizationRelationships.Where(rel => rel.FromId == organization.IngressId);
                var toRelationships = data.OrganizationRelationships.Where(rel => rel.ToId == organization.IngressId);
                foreach (var fromRelationship in fromRelationships)
                {
                    fromRelationship.FromId = organization.EgressId;
                }
                foreach (var toRelationship in toRelationships)
                {
                    toRelationship.ToId = organization.EgressId;
                }
            });
            
            try
            {
                await Task.WhenAll(createOrganizations);
            }
            catch (Exception ex)
            {
                IEnumerable<Exception> exceptions = createOrganizations
                    .Where(createOrganization => createOrganization.IsFaulted)
                    .Select(createOrganization => createOrganization.Exception);
                if (!exceptions.Any())
                {
                    exceptions = new[] { ex };
                }
                throw new EgressException(exceptions);
            }
        }
    }
}