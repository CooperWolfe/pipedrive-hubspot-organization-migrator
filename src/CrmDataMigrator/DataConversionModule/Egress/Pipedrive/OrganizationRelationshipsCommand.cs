using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Pipedrive;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress.Pipedrive
{
    internal class OrganizationRelationshipsCommand : IPipedriveCommand
    {
        private readonly HttpClient httpClient;

        public OrganizationRelationshipsCommand(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Pipedrive;
            var createOrganizationRelationships = data.OrganizationRelationships.Select(async relationship =>
            {
                var createOrganizationRelationship = CreateOrganizationRelationshipRequest
                    .FromProprietaryOrganizationRelationship(relationship);

                var httpRequest = new HttpRequestMessage(
                    HttpMethod.Post,
                    $"https://{configuration.CompanyDomain}.pipedrive.com/v1/organizationRelationships?api_token={configuration.ApiToken}");
                var jsonBody = JsonSerializer.Serialize(createOrganizationRelationship);
                httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
                
                var httpResponse = await httpClient.SendAsync(httpRequest);
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception($"Error creating organization relationship ({relationship.FromId} => {relationship.ToId}): {httpResponse.StatusCode}");
                }
            });
            
            try
            {
                await Task.WhenAll(createOrganizationRelationships);
            }
            catch (Exception ex)
            {
                IEnumerable<Exception> exceptions = createOrganizationRelationships
                    .Where(createOrganization => createOrganization.IsFaulted)
                    .Select(createOrganization => createOrganization.Exception);
                if (!exceptions.Any())
                {
                    exceptions = new[] { ex };
                }
                throw new EgressException(exceptions);
            }
        }
    }
}