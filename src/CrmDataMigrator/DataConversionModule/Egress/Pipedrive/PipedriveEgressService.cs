using System.Collections.Generic;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Egress.Pipedrive
{
    internal class PipedriveEgressService : CrmEgressService
    {
        public PipedriveEgressService(IEnumerable<IPipedriveCommand> commands) : base(commands)
        {
        }

        public override CrmService CrmService => CrmService.Pipedrive;
    }
}