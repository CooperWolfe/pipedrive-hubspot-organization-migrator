using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.DataConversionModule.Model.Hubspot;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress.Hubspot
{
    internal class CompaniesCommand : IHubspotCommand
    {
        private readonly HttpClient httpClient;

        public CompaniesCommand(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Hubspot;
            var createCompanies = data.Organizations.Select(async organization => 
            {
                var createCompany = CreateCompany.FromProprietaryOrganization(organization);

                var httpRequest = new HttpRequestMessage(
                    HttpMethod.Post,
                    $"https://api.hubapi.com/crm/v3/objects/companies?hapikey={configuration.ApiToken}");
                var jsonBody = JsonSerializer.Serialize(createCompany);
                httpRequest.Content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

                var httpResponse = await httpClient.SendAsync(httpRequest);
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception($"Error creating company ({organization.Name}): {httpResponse.StatusCode}");
                }

                await using var responseContent = await httpResponse.Content.ReadAsStreamAsync();
                var readCompany = await JsonSerializer.DeserializeAsync<ReadCompany>(responseContent);

                organization.EgressId = readCompany.Id;
                var fromRelationshipsToUpdate = data.OrganizationRelationships.Where(rel => rel.FromId == organization.IngressId);
                var toRelationshipsToUpdate = data.OrganizationRelationships.Where(rel => rel.ToId == organization.IngressId);
                foreach (var relationship in fromRelationshipsToUpdate)
                {
                    relationship.FromId = organization.EgressId;
                }
                foreach (var relationship in toRelationshipsToUpdate)
                {
                    relationship.ToId = organization.EgressId;
                }
            });

            try
            {
                await Task.WhenAll(createCompanies);
            }
            catch (Exception ex)
            {
                IEnumerable<Exception> exceptions = createCompanies
                    .Where(task => task.IsFaulted)
                    .Select(task => task.Exception);
                if (!exceptions.Any())
                {
                    exceptions = new[] { ex };
                }
                throw new EgressException(exceptions);
            }
        }
    }
}