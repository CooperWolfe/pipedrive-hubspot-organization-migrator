using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress.Hubspot
{
    internal class CompanyAssociationsCommand : IHubspotCommand
    {
        private readonly HttpClient httpClient;

        public CompanyAssociationsCommand(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task ExecuteAsync(ConfigurationRequest configurationRequest, CrmData data)
        {
            var configuration = configurationRequest.Hubspot;

            var createCompanyAssociations = data.OrganizationRelationships.Select(async relationship =>
            {
                string relationshipType = relationship.FromRole switch
                {
                    RelationshipRole.Child => "child_to_parent_company",
                    RelationshipRole.Parent => "parent_to_child_company",
                    _ => null
                };
                if (relationshipType == null)
                {
                    return;
                }

                var httpRequest = new HttpRequestMessage(
                    HttpMethod.Put,
                    $"https://api.hubapi.com/crm/v3/objects/companies/{relationship.FromId}/associations/company/{relationship.ToId}/{relationshipType}" +
                        $"?hapikey={configuration.ApiToken}");
                var httpResponse = await httpClient.SendAsync(httpRequest);
                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception($"Error creating company association ({relationship.FromId} => {relationship.ToId}): {httpResponse.StatusCode}");
                }
            });

            try
            {
                await Task.WhenAll(createCompanyAssociations);
            }
            catch (Exception ex)
            {
                IEnumerable<Exception> exceptions = createCompanyAssociations
                    .Where(task => task.IsFaulted)
                    .Select(task => task.Exception);
                if (!exceptions.Any())
                {
                    exceptions = new[] { ex };
                }
                throw new EgressException(exceptions);
            }
        }
    }
}