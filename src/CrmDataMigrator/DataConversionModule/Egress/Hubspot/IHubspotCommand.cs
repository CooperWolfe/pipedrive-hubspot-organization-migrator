using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Model;
using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.DataConversionModule.Egress.Hubspot
{
    internal interface IHubspotCommand : ICrmCommand
    {
    }
}