using System.Collections.Generic;
using static CrmDataMigrator.Grpc.ConfigurationRequest.Types;

namespace CrmDataMigrator.DataConversionModule.Egress.Hubspot
{
    internal class HubspotEgressService : CrmEgressService
    {
        public HubspotEgressService(IEnumerable<IHubspotCommand> commands) : base(commands)
        {
        }

        public override CrmService CrmService => CrmService.Hubspot;
    }
}