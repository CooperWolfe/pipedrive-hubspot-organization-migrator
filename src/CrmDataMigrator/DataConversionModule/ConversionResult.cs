using System;
using CrmDataMigrator.DataConversionModule.Exceptions;

namespace CrmDataMigrator.DataConversionModule
{
    public class ConversionResult
    {
        private ConversionResult()
        {
        }

        public bool IsSuccessful { get; private set; }
        public DataConversionException Exception { get; private set; }

        internal static ConversionResult Fail(IngressException exception)
        {
            return new ConversionResult
            {
                IsSuccessful = false,
                Exception = new DataConversionException(exception)
            };
        }

        internal static ConversionResult Fail(EgressException exception)
        {
            return new ConversionResult
            {
                IsSuccessful = false,
                Exception = new DataConversionException(exception)
            };
        }

        internal static ConversionResult Success()
        {
            return new ConversionResult
            {
                IsSuccessful = true
            };
        }
    }
}