using CrmDataMigrator.DataConversionModule;
using CrmDataMigrator.DataConversionModule.Egress;
using CrmDataMigrator.DataConversionModule.Egress.Hubspot;
using CrmDataMigrator.DataConversionModule.Egress.Pipedrive;
using CrmDataMigrator.DataConversionModule.Ingress;
using CrmDataMigrator.DataConversionModule.Ingress.Hubspot;
using CrmDataMigrator.DataConversionModule.Ingress.Pipedrive;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DataConversionServiceCollectionExtensions
    {
        public static IServiceCollection AddDataConversionModule(this IServiceCollection services)
        {
            services.AddSingleton<IDataConversionService, DataConversionService>();
            services.AddSingleton<IIngressService, IngressService>();
            services.AddSingleton<IEgressService, EgressService>();

            services.AddScoped<ICrmIngressService, HubspotIngressService>();
            services.AddScoped<ICrmIngressService, PipedriveIngressService>();
            services.AddScoped<ICrmEgressService, HubspotEgressService>();
            services.AddScoped<ICrmEgressService, PipedriveEgressService>();

            services.AddHttp<IHubspotQuery, CompaniesQuery>();
            services.AddHttp<IHubspotQuery, CompanyAssociationsQuery>();

            services.AddHttp<IPipedriveQuery, OrganizationsQuery>();
            services.AddHttp<IPipedriveQuery, OrganizationRelationshipsQuery>();

            services.AddHttp<IHubspotCommand, CompaniesCommand>();
            services.AddHttp<IHubspotCommand, CompanyAssociationsCommand>();

            services.AddHttp<IPipedriveCommand, OrganizationsCommand>();
            services.AddHttp<IPipedriveCommand, OrganizationRelationshipsCommand>();

            return services;
        }
    }
}