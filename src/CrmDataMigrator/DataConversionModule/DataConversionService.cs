using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule.Egress;
using CrmDataMigrator.DataConversionModule.Ingress;

namespace CrmDataMigrator.DataConversionModule
{
    internal class DataConversionService : IDataConversionService
    {
        private readonly IIngressService ingressService;
        private readonly IEgressService egressService;

        public DataConversionService(
            IIngressService ingressService,
            IEgressService egressService)
        {
            this.ingressService = ingressService;
            this.egressService = egressService;
        }

        public async Task<ConversionResult> ConvertAsync(Conversion message)
        {
            var ingressResponse = await ingressService.ImportAsync(message.Input);
            if (!ingressResponse.IsSuccessful)
            {
                return ConversionResult.Fail(ingressResponse.Exception);
            }
            var ingressData = ingressResponse.Data;

            var egressResponse = await egressService.ExportAsync(message.Output, ingressData);
            if (!egressResponse.IsSuccessful)
            {
                return ConversionResult.Fail(egressResponse.Exception);
            }

            return ConversionResult.Success();
        }
    }
}