using System.Threading.Tasks;

namespace CrmDataMigrator.DataConversionModule
{
    public interface IDataConversionService
    {
        Task<ConversionResult> ConvertAsync(Conversion message);
    }
}