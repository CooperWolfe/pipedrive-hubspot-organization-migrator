using System.Collections.Generic;

namespace CrmDataMigrator.DataConversionModule.Model
{
    internal class CrmData
    {
        public Organization[] Organizations { get; set; }
        public OrganizationRelationship[] OrganizationRelationships { get; set; }
    }
}