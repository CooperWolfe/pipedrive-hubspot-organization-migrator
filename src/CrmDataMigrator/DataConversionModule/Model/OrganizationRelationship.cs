using System;

namespace CrmDataMigrator.DataConversionModule.Model
{
    internal class OrganizationRelationship : IEquatable<OrganizationRelationship>
    {
        public string FromId { get; set; }
        public string ToId { get; set; }
        public RelationshipRole FromRole { get; set; }
        public RelationshipRole ToRole { get; set; }

        public bool Equals(OrganizationRelationship other)
        {
            if (other == null) return false;
            if (other == this) return true;

            return other.FromId == FromId
                    && other.ToId == ToId
                    && other.FromRole == FromRole
                    && other.ToRole == ToRole
                || other.ToId == FromId
                    && other.FromId == ToId
                    && other.ToRole == FromRole
                    && other.FromRole == ToRole;
        }

        public override int GetHashCode()
        {
            return FromId.GetHashCode()
                + ToId.GetHashCode()
                + FromRole.GetHashCode()
                + ToRole.GetHashCode();
        }
    }
}