using System;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class PipedriveDateTimeConverter : JsonConverter<DateTime>
    {
        private const string FormatString = "yyyy-MM-dd HH:mm:ss";

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return DateTime.ParseExact(reader.GetString(), FormatString, CultureInfo.InvariantCulture);
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString(FormatString, CultureInfo.InvariantCulture));
        }
    }
}