using System.Text.Json.Serialization;
using ProprietaryOrganizationRelationship = CrmDataMigrator.DataConversionModule.Model.OrganizationRelationship;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class CreateOrganizationRelationshipRequest
    {
        private CreateOrganizationRelationshipRequest()
        {
        }

        [JsonPropertyName("org_id")]
        public long OrganizationId { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("rel_owner_org_id")]
        public long RelationshipOwnerOrganizationId { get; set; }

        [JsonPropertyName("rel_linked_org_id")]
        public long RelationshipLinkedOrganizationId { get; set; }

        internal static CreateOrganizationRelationshipRequest FromProprietaryOrganizationRelationship(ProprietaryOrganizationRelationship relationship)
        {
            return relationship.FromRole switch
            {
                RelationshipRole.Parent => new CreateOrganizationRelationshipRequest
                {
                    OrganizationId = long.Parse(relationship.FromId),
                    RelationshipOwnerOrganizationId = long.Parse(relationship.FromId),
                    RelationshipLinkedOrganizationId = long.Parse(relationship.ToId),
                    Type = "parent"
                },
                RelationshipRole.Child => new CreateOrganizationRelationshipRequest
                {
                    OrganizationId = long.Parse(relationship.ToId),
                    RelationshipOwnerOrganizationId = long.Parse(relationship.ToId),
                    RelationshipLinkedOrganizationId = long.Parse(relationship.FromId),
                    Type = "parent"
                },
                _ => new CreateOrganizationRelationshipRequest
                {
                    OrganizationId = long.Parse(relationship.FromId),
                    RelationshipOwnerOrganizationId = long.Parse(relationship.FromId),
                    RelationshipLinkedOrganizationId = long.Parse(relationship.ToId),
                    Type = "related"
                }
            };
        }
    }
}