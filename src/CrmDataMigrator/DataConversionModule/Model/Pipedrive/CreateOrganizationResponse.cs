using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class CreateOrganizationResponse
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }

        [JsonPropertyName("data")]
        public ReadOrganization Data { get; set; }
    }
}