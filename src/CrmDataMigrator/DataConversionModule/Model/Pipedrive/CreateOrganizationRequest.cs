using System;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class CreateOrganizationRequest
    {
        private CreateOrganizationRequest()
        {
        }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("owner_id")]
        public int? OwnerId { get; set; }

        // 1 = private, 3 = public
        [JsonPropertyName("visible_to")]
        public int? VisibleTo { get; set; }

        [JsonPropertyName("add_time")]
        public DateTime? AddTime { get; set; }

        internal static CreateOrganizationRequest FromProprietaryOrganization(Organization organization)
        {
            return new CreateOrganizationRequest
            {
                Name = organization.Name
            };
        }
    }
}