using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class PipedriveIntToBoolConverter : JsonConverter<bool>
    {
        private const string FormatString = "yyyy-MM-dd HH:mm:ss";

        public override bool Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return reader.GetInt32() > 0;
        }

        public override void Write(Utf8JsonWriter writer, bool value, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(value ? 1 : 0);
        }
    }
}