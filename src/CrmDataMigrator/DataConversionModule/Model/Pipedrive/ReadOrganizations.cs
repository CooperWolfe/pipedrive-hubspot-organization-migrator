using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class ReadOrganizations
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }

        [JsonPropertyName("data")]
        public IEnumerable<ReadOrganization> Data { get; set; }
    }
}
