using System.Collections.Generic;
using System.Text.Json.Serialization;
using ProprietaryOrganizationRelationship = CrmDataMigrator.DataConversionModule.Model.OrganizationRelationship;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class ReadOrganizationRelationships
    {
        [JsonPropertyName("success")]
        public bool Success { get; set; }

        [JsonPropertyName("data")]
        public IEnumerable<OrganizationRelationship> Data { get; set; }
    }

    internal class OrganizationRelationship
    {
        [JsonPropertyName("rel_owner_org_id")]
        public OrganizationId RelationshipOwnerOrganizationId { get; set; }

        [JsonPropertyName("rel_linked_org_id")]
        public OrganizationId RelationshipLinkedOrganizationId { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        internal ProprietaryOrganizationRelationship ToProprietaryOrganizationRelationship()
        {
            return new ProprietaryOrganizationRelationship
            {
                FromId = RelationshipOwnerOrganizationId.Value.ToString(),
                ToId = RelationshipLinkedOrganizationId.Value.ToString(),
                FromRole = Type switch {
                    "parent" => RelationshipRole.Parent,
                    "daughter" => RelationshipRole.Child,
                    _ => RelationshipRole.Other
                },
                ToRole = Type switch {
                    "parent" => RelationshipRole.Child,
                    "daughter" => RelationshipRole.Parent,
                    _ => RelationshipRole.Other
                }
            };
        }
    }

    internal class OrganizationId
    {
        [JsonPropertyName("value")]
        public long Value { get; set; }
    }
}
