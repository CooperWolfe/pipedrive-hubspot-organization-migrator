using System;
using System.Text.Json.Serialization;
using ProprietaryOrganization = CrmDataMigrator.DataConversionModule.Model.Organization;

namespace CrmDataMigrator.DataConversionModule.Model.Pipedrive
{
    internal class ReadOrganization
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("company_id")]
        public long CompanyId { get; set; }

        [JsonPropertyName("owner_id")]
        public ReadOrganizationOwner Owner { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("active_flag")]
        public bool ActiveFlag { get; set; }

        [JsonPropertyName("update_time")]
        [JsonConverter(typeof(PipedriveDateTimeConverter))]
        public DateTime UpdateTime { get; set; }

        [JsonPropertyName("add_time")]
        [JsonConverter(typeof(PipedriveDateTimeConverter))]
        public DateTime AddTime { get; set; }

        [JsonPropertyName("cc_email")]
        public string CcEmail { get; set; }

        internal ProprietaryOrganization ToProprietaryOrganization()
        {
            return new ProprietaryOrganization(ingressId: Id.ToString(), name: Name);
        }
    }

    internal class ReadOrganizationOwner
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("has_pic")]
        [JsonConverter(typeof(PipedriveIntToBoolConverter))]
        public bool HasPic { get; set; }

        [JsonPropertyName("pic_hash")]
        public string PicHash { get; set; }

        [JsonPropertyName("active_flag")]
        public bool ActiveFlag { get; set; }

        [JsonPropertyName("value")]
        public long Value { get; set; }
    }
}