namespace CrmDataMigrator.DataConversionModule.Model
{
    internal enum RelationshipRole
    {
        Parent,
        Child,
        Other
    }
}