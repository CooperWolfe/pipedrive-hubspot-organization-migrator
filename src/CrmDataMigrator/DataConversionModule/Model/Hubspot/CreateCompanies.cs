using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Hubspot
{
    internal class CreateCompanies
    {
        private CreateCompanies() {}

        [JsonPropertyName("inputs")]
        public IEnumerable<CreateCompany> Inputs { get; set; }

        internal static CreateCompanies FromProprietaryOrganizations(IEnumerable<Organization> organizations)
        {
            return new CreateCompanies
            {
                Inputs = organizations.Select(CreateCompany.FromProprietaryOrganization)
            };
        }
    }
}