using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Hubspot
{
    internal class ReadCompanyAssociations
    {
        [JsonPropertyName("results")]
        public IEnumerable<ReadCompanyAssociation> Results { get; set; }
    }

    internal class ReadCompanyAssociation
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        internal OrganizationRelationship ToProprietaryOrganizationRelationship(string fromId)
        {
            return new OrganizationRelationship
            {
                FromId = fromId,
                ToId = Id,
                FromRole = Type switch
                {
                    "child_to_parent_company" => RelationshipRole.Child,
                    "parent_to_child_company" => RelationshipRole.Parent,
                    _ => RelationshipRole.Other
                },
                ToRole = Type switch
                {
                    "child_to_parent_company" => RelationshipRole.Parent,
                    "parent_to_child_company" => RelationshipRole.Child,
                    _ => RelationshipRole.Other
                }
            };
        }
    }
}
