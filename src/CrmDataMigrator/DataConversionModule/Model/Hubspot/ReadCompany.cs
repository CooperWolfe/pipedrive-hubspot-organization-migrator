using System;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Hubspot
{
    internal class ReadCompany
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("properties")]
        public ReadCompanyProperties Properties { get; set; }

        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonPropertyName("archived")]
        public bool Archived { get; set; }

        internal Organization ToProprietaryOrganization()
        {
            return new Organization(ingressId: Id, name: Properties?.Name);
        }
    }

    internal class ReadCompanyProperties
    {
        [JsonPropertyName("createdate")]
        public DateTime CreateDate { get; set; }

        [JsonPropertyName("domain")]
        public string Domain { get; set; }

        [JsonPropertyName("hs_lastmodifieddate")]
        public DateTime LastModifiedDate { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}