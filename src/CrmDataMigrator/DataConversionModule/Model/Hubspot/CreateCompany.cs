using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Hubspot
{
    internal class CreateCompany
    {
        private CreateCompany() {}

        [JsonPropertyName("properties")]
        public CreateCompanyProperties Properties { get; set; }

        internal static CreateCompany FromProprietaryOrganization(Organization organization)
        {
            return new CreateCompany
            {
                Properties = CreateCompanyProperties.FromProprietaryOrganization(organization)
            };
        }
    }

    internal class CreateCompanyProperties
    {
        private CreateCompanyProperties() {}

        [JsonPropertyName("city")]
        public string City { get; set; }
        
        [JsonPropertyName("domain")]
        public string Domain { get; set; }
        
        [JsonPropertyName("industry")]
        public string Industry { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }

        [JsonPropertyName("state")]
        public string State { get; set; }

        internal static CreateCompanyProperties FromProprietaryOrganization(Organization organization)
        {
            return new CreateCompanyProperties
            {
                Name = organization.Name
            };
        }
    }
}