using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CrmDataMigrator.DataConversionModule.Model.Hubspot
{
    internal class ReadCompanies
    {
        [JsonPropertyName("results")]
        public IEnumerable<ReadCompany> Results { get; set; }
    }
}
