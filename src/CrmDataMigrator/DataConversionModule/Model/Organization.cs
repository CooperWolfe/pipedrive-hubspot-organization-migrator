using System;

namespace CrmDataMigrator.DataConversionModule.Model
{
    internal class Organization
    {
        public Organization(string ingressId, string name)
        {
            IngressId = ingressId;
            Name = name;
        }

        public string IngressId { get; }
        public string EgressId { get; set; }
        public string Name { get; set; }
    }
}