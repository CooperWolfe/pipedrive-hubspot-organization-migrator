using System;
using System.Collections.Generic;

namespace CrmDataMigrator.DataConversionModule.Exceptions
{
    public class EgressException : AggregateException
    {
        public EgressException(string errorMessage) : base(errorMessage)
        {
        }

        public EgressException(Exception exception) : base(exception)
        {
        }

        public EgressException(IEnumerable<Exception> exceptions) : base(exceptions)
        {
        }
    }
}