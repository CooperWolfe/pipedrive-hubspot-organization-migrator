using System;

namespace CrmDataMigrator.DataConversionModule.Exceptions
{
    public class DataConversionException : Exception
    {
        internal DataConversionException(IngressException exception) : base("Experienced an error while importing data", exception)
        {
        }

        internal DataConversionException(EgressException exception) : base("Experienced an error while exporting data", exception)
        {
        }
    }
}