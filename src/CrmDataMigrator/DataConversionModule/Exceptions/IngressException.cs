using System;
using System.Collections.Generic;

namespace CrmDataMigrator.DataConversionModule.Exceptions
{
    internal class IngressException : AggregateException
    {
        public IngressException(string errorMessage) : base(errorMessage)
        {
        }

        public IngressException(Exception exception) : base(exception)
        {
        }

        public IngressException(IEnumerable<Exception> exceptions) : base(exceptions)
        {            
        }
    }
}