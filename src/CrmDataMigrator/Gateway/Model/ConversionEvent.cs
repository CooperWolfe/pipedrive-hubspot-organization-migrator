using CrmDataMigrator.DataConversionModule;

namespace CrmDataMigrator.Gateway.Model
{
    class ConversionEvent
    {
        string EventType { get; set; }
        Conversion Data { get; set; }
    }
}