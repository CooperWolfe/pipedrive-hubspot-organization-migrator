using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.Gateway.Validators
{
    public interface IConfigurationRequestValidator
    {
        ValidationResult Validate(ConfigurationRequest configurationRequest);
    }
}