using CrmDataMigrator.Grpc;

namespace CrmDataMigrator.Gateway.Validators
{
    class ConfigurationRequestValidator : IConfigurationRequestValidator
    {
        public ValidationResult Validate(ConfigurationRequest configurationRequest)
        {
            if (!CrmMatchesConfiguration(configurationRequest))
            {
                return ValidationResult.Failure(ValidationResult.ValidationFailure.MismatchingCrmConfiguration);
            }

            return ValidationResult.Success();
        }

        private bool CrmMatchesConfiguration(ConfigurationRequest configurationRequest)
        {
            switch (configurationRequest.CrmService)
            {
                case ConfigurationRequest.Types.CrmService.Pipedrive:
                    return configurationRequest.ConfigurationCase == ConfigurationRequest.ConfigurationOneofCase.Pipedrive;
                case ConfigurationRequest.Types.CrmService.Hubspot:
                    return configurationRequest.ConfigurationCase == ConfigurationRequest.ConfigurationOneofCase.Hubspot;
                default:
                    return false;
            }
        }
    }
}