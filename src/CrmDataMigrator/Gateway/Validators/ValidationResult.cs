namespace CrmDataMigrator.Gateway.Validators
{
    public class ValidationResult
    {
        private ValidationResult()
        {
        }

        public bool IsSuccessful { get; private set; }
        public ValidationFailure? FailureReason { get; private set; }

        public static ValidationResult Success()
        {
            return new ValidationResult
            {
                IsSuccessful = true
            };
        }

        public static ValidationResult Failure(ValidationFailure failureReason)
        {
            return new ValidationResult
            {
                IsSuccessful = false,
                FailureReason = failureReason
            };
        }

        public enum ValidationFailure
        {
            MismatchingCrmConfiguration
        }
    }
}