using System.Threading.Tasks;
using CrmDataMigrator.CrmModule.AccountVerification;
using CrmDataMigrator.Gateway.Validators;
using CrmDataMigrator.Grpc;
using Grpc.Core;
using static CrmDataMigrator.CrmModule.AccountVerification.AccountVerificationResponse;
using static CrmDataMigrator.Gateway.Validators.ValidationResult;

namespace CrmDataMigrator.Gateway.Services
{
    public class ConfigurationService : Configuration.ConfigurationBase
    {
        private readonly IConfigurationRequestValidator configurationRequestValidator;
        private readonly IAccountVerifier accountVerifier;

        public ConfigurationService(
            IConfigurationRequestValidator configurationRequestValidator,
            IAccountVerifier accountVerifier)
        {
            this.configurationRequestValidator = configurationRequestValidator;
            this.accountVerifier = accountVerifier;
        }

        public override async Task<Success> Configure(ConfigurationRequest request, ServerCallContext context)
        {
            var validationResult = configurationRequestValidator.Validate(request);
            if (!validationResult.IsSuccessful)
            {
                switch (validationResult.FailureReason)
                {
                    case ValidationFailure.MismatchingCrmConfiguration:
                        throw new RpcException(new Status(StatusCode.InvalidArgument, "The CRM selected does not match the configuration object passed in"));
                    default:
                        throw new RpcException(new Status(StatusCode.Unknown, "An unknown error occurred while validating the configuration"));
                }
            }

            var verificationResponse = await accountVerifier.VerifyAccountAsync(request);
            if (!verificationResponse.IsSuccessful)
            {
                switch (verificationResponse.FailureReason)
                {
                    case VerificationFailure.Unauthorized:
                        throw new RpcException(new Status(StatusCode.PermissionDenied, "Could not contact the CRM service due to invalid configuration"));
                    case VerificationFailure.CouldNotContactService:
                        throw new RpcException(new Status(StatusCode.Unavailable, "This CRM service is currently unavailable"));
                    default:
                        throw new RpcException(new Status(StatusCode.Unknown, "An unknown error occurred while verifying the account"));
                }
            }

            return new Success();
        }
    }
}