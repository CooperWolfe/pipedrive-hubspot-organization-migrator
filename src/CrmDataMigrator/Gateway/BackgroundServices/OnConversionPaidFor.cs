using System;
using System.Threading;
using System.Threading.Tasks;
using CrmDataMigrator.DataConversionModule;
using CrmDataMigrator.DataConversionModule.Exceptions;
using CrmDataMigrator.MessageBus.Consumer;
using Microsoft.Extensions.Logging;

namespace CrmDataMigrator.Gateway.BackgroundServices
{
    class OnConversionPaidFor : MessageBusConsumerBackgroundService<Conversion>
    {
        private readonly IDataConversionService dataConversionService;

        OnConversionPaidFor(
            ILogger<MessageBusConsumerBackgroundService<Conversion>> logger,
            IMessageConsumerFactory consumerFactory,
            IDataConversionService dataConversionService)
            : base("data-conversion.conversion", logger, consumerFactory)
        {
            this.dataConversionService = dataConversionService;
        }

        protected override async Task<bool> OnConsume(Conversion message, CancellationToken stoppingToken)
        {
            var conversionResult = await dataConversionService.ConvertAsync(message);
            if (!conversionResult.IsSuccessful)
            {
                throw conversionResult.Exception;
            }
            return true;
        }

        protected override async Task<bool> OnException(Exception exception)
        {
            await Task.Yield();
            return exception.GetType() == typeof(DataConversionException);
        }
    }
}