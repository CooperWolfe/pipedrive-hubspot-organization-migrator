# CRM Data Migrator

I originally wanted to make a SaaS application to migrate data between CRM platforms,
but a larger company beat me to it with enough features and partnerships to discourage
me from completing the project. It was fun, though. I learned about some technologies
I'd been wanting to try out and enjoyed designing the architecture.

> Copyright of Cooper Wolfe circa 2021
